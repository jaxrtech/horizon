import * as React from 'react';
import { Subscriber, Broadcast } from 'react-broadcast';
import { Data } from 'slate';

class DataSubscriberComponent extends Subscriber<Data> { }

export const DataSubscriber = (props: Subscriber.Props<Data>) =>
  <Subscriber {...props} channel={'data'} />;

class DataBroadcastComponent extends Broadcast<Data> { }

export const DataBroadcast = (props: Broadcast.Props<Data>) =>
  <Broadcast {...props} channel={'data'} />;
