import { Editor } from 'slate-react';
import { Mark, Node } from 'slate';

export interface EditorProps {
  attributes: Array<any>;
  editor: Editor;
  node: Node;
  mark: Mark;
  className?: string;
}