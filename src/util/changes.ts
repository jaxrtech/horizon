import { Change, Value } from 'slate';
import * as Immutable from 'immutable';

type uuid = string;

export interface UnodeEntry<TNode extends Unode> {
  uid: uuid;
  unode: TNode;
}

/** hz-* */
export interface UnodeProps {
  type: string;
}
export interface Unode extends UnodeProps, Immutable.Map<string, any> { }
// With the way Immutable.Record works, you need to create the concrete subtype, in-order to have the right parameters

/** hz-num */
export interface NumUnodeProps extends UnodeProps {
  type: 'hz-num';
  value: number;
  name?: string;
}
const NumUnodeDefault: NumUnodeProps = {
  type: 'hz-num',
  value: NaN,
  name: undefined,
};
export interface NumUnode extends NumUnodeProps, Immutable.Map<string, any> { }
const NumUnodeRecord = Immutable.Record(NumUnodeDefault);
export const NumUnode = (attrs: NumUnodeProps) => new NumUnodeRecord(attrs);

/** hz-ref */
export interface RefUnodeProps extends UnodeProps {
  type: 'hz-ref';
  ref: uuid | null;
}
const RefUnodeDefault: RefUnodeProps = {
  type: 'hz-ref',
  ref: null,
};
export interface RefUnode extends RefUnodeProps, Immutable.Map<string, any> { }
const RefUnodeRecord = Immutable.Record(RefUnodeDefault);
export const RefUnode = (attrs: RefUnodeProps) => new RefUnodeRecord(attrs);

/** functions */
export function setUnode(change: Change, uid: string, unode: Unode) {
  console.log('setUnode', { [uid]: unode });

  const key = change.value.document.key;  
  const old = change.value.document.data;
  const next = old.update(uid, _ => unode);

  change.call(markAsEditor); // HACK?
  change.setNodeByKey(key, { data: next });
}

export function setUidValue(change: Change, uid: string, n: number) {
  console.log('setUid', { [uid]: n });
  const key = change.value.document.key;
  const old = change.value.document.data;
  
  let next;
  if (old.has(uid)) {
    next = old.update(uid, u => u.set('value', n));
  } else {
    next = old.update(uid, _ => NumUnode({ type: 'hz-num', value: n }));
  }

  change.call(markAsEditor); // HACK?
  change.setNodeByKey(key, { data: next });
}

export function setUidName(change: Change, uid: string, name: string) {
  console.log('setUidName', { [uid]: name });
  const key = change.value.document.key;
  const old = change.value.document.data;
  
  let next;
  if (old.has(uid)) {
    next = old.update(uid, n => n.set('name', name));
  } else {
    // TODO: Design decision: should you be able to create a unode without a value?
    throw new Error('cannot create unode without value');
  }

  change.call(markAsEditor); // HACK?
  change.setNodeByKey(key, { data: next });
}

export function getUnode<T extends Unode>(value: Value, uid: string): T {
  const x = value.document.data.get(uid) as T;
  console.log('getUnode', { [uid]: x });
  return x;
}

export function getUnodeByName(value: Value, name: string): UnodeEntry<NumUnode> | null {
  console.log('getUnodeByName', value.document.data);
  const x = value.document.data.findEntry((v, k) =>
    (v as NumUnode).name === name) as [string, NumUnode] | undefined;

  if (typeof x === 'undefined') {
    return null;
  }

  const [uid, unode] = x;
  return { uid, unode };
}

export function getUnodeValue(value: Value, uid: string): number {
  const x = (getUnode(value, uid) as NumUnode).value;
  console.log('getUid', { [uid]: x });
  return x;
}

export function removeUid(change: Change, uid: string): number {
  const n = getUnodeValue(change.value, uid);
  console.log('removeUid', { [uid]: n });

  const key = change.value.document.key;
  const old = change.value.document.data;
  change.setNodeByKey(key, { data: old.remove(uid) });
  change.call(markAsEditor); // HACK?

  return n;
}

export function markAsEditor(change: Change) {
  change.setOperationFlag('editor', true);
}

export function unmarkAsEditor(change: Change) {
  change.unsetOperationFlag('editor');
}