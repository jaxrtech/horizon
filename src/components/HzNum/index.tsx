import * as React from 'react';
import { Slider, Popover, Position, PopoverInteractionKind } from '@blueprintjs/core';
import { Inline, Value, Change, Node, Mark } from 'slate';
import { Editor } from 'slate-react';

import * as Changes from '../../util/changes';
import { NumUnode } from '../../util/changes';
import { NumberSlider } from '../NumberSlider';
import { EditorProps } from '../../util/editor-props';
import './index.css';

interface HzNumStateProps extends EditorProps {
  value: number;
}

interface HzNumState {
  uid: string;
  value: number;
  name: string | undefined;
}

export class HzNum extends React.Component<HzNumStateProps, HzNumState> {

  static shouldNodeComponentUpdate(previousProps: any, nextProps: any) {
    return true;
  }

  constructor(props: HzNumStateProps) {
    super(props);
    this.state = { uid: this._uid, value: this._value || NaN, name: this._name };
    console.log('NumberSlider/ctor', this.props, this.state);
  }

  private get _uid() {
    return this.props.node.data.get('uid');
  }

  private get _editorValue() {
    return this.props.editor.value;
  }

  private get _value() {
    const uid = this._uid;
    const x = Changes.getUnodeValue(this._editorValue, uid);
    if (typeof x === 'undefined') {
      console.error('invalid uid', uid);
      return;
    }

    console.log('getValue()', uid, x);
    return x;
  }

  private get _name() {
    const name = Changes.getUnode<NumUnode>(this._editorValue, this._uid).name;
    console.log('NumberSlider/name', name);
    return name || '';
  }

  handleChangeValue = (n: number) => {
    this.setState({ value: n });    
    
    const child = (this.props.node as any as Inline).getTexts().first();
    const changes = this.props.editor.change((change: Change) => {
      change
        .call(Changes.setUidValue, this._uid, n)
        .moveToRangeOf(child)
        .insertText(n.toFixed(0).toString());
    });
  }

  handleChangeName = (name: string) => {
    console.log('handleChangeName', name);
    this.setState({ name });
    const changes = this.props.editor.change((change: Change) => {
      change.call(Changes.setUidName, this._uid, name);
    });
  }

  render() {
    return (
      <span {...this.props.attributes}>
        <NumberSlider
          className={this.props.className}
          name={this.state.name}
          value={this.state.value}
          onChangeName={this.handleChangeName}
          onChangeValue={this.handleChangeValue}
        >
          {this.props.children}
        </NumberSlider>
      </span>
    );
  }
}