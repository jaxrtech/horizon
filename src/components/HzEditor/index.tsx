import * as React from 'react';
import * as Immutable from 'immutable';
import { v4 as uuid } from 'uuid';

import { Value, Change, Operation, Range, Node, Text, Inline, Document } from 'slate';
import { Editor } from 'slate-react';
import { RangeAttrs } from 'slate/models/range';
import StickyInlines from 'slate-sticky-inlines';

import * as Changes from '../../util/changes';
import { NumberSlider } from '../NumberSlider';

import './index.css';
import { LabeledInput } from '../LabeledInput';
import { BorderedSpan } from '../BorderedSpan';
import SuggestionsPlugin from '@jaxrtech/slate-suggestions';
import * as INITIAL_VALUE_JSON from '../../util/initial-value.json';
import { Unode, RefUnode, UnodeProps, NumUnode, NumUnodeProps, RefUnodeProps } from '../../util/changes';
import { HzNum } from '../HzNum';
import { HzRef } from '../HzRef';

// Create our initial value...
const INITIAL_VALUE = Value.fromJSON(INITIAL_VALUE_JSON);

const BLOCK_TAGS = {
  p: 'paragraph',
  blockquote: 'quote',
  pre: 'code',
};

const MARK_TAGS = {
  em: 'italic',
  strong: 'bold',
  u: 'underline',
};

interface HzEditorProps { }

interface HzEditorState {
  value: Value;
  search: string;
  nonce: number;
  suggestions: Array<{ key: string, value: string, suggestion: string | JSX.Element }>;
}

type GetCurrentWordType = { start: number; end: number } | number | undefined;
function getCurrentWord(text: string, index: number, initialIndex: number): GetCurrentWordType {
  if (index === initialIndex) {
    return {
      start: getCurrentWord(text, index - 1, initialIndex) as number,
      end: getCurrentWord(text, index + 1, initialIndex) as number
    };
  }
  if (text[index] === ' ' || text[index] === '@' || text[index] === undefined) {
    return index;
  }
  if (index < initialIndex) {
    return getCurrentWord(text, index - 1, initialIndex);
  }
  if (index > initialIndex) {
    return getCurrentWord(text, index + 1, initialIndex);
  }

  return undefined;
}

export class HzEditor extends React.Component<HzEditorProps, HzEditorState> {

  suggestionsPlugin: any;
  plugins: any[];

  private static getDocumentFromStorage() {
    const content = localStorage.getItem('content');
    if (!content) {
      return null;
    }

    try {
      const json = JSON.parse(content);
      return Value.fromJSON(json);
    } catch (e) {
      if (e instanceof SyntaxError) {
        return null;
      }

      throw e;
    }
  }

  constructor() {
    super({});
    
    // const str = JSON.stringify(html.deserialize('<p>Hello, <strong>Josh and Sydney</strong>!</p>'));
    // localStorage.setItem('content', str);

    // const value = HzEditor.getDocumentFromStorage() || initialValue;
    const initialData = INITIAL_VALUE.document.data.mapEntries(([k, v]: [string, any]) => {
      const make = (n: UnodeProps) => {
        switch (n.type as string) {
          case 'hz-num': return NumUnode(n as NumUnodeProps);
          case 'hz-ref': return RefUnode(n as RefUnodeProps);
          default: throw new Error(`unknown unode of type '${n.type}'`);
        }
      };

      return [k, make(v)];
    }).toMap();

    const actualValue = INITIAL_VALUE.update('document', (d: Document) => d.set('data', initialData));
    const { value } = HzEditor.initialTransform(actualValue);
    console.log(value);

    this.state = {
      nonce: 0,
      value,
      search: '',
      suggestions: [
        {
          key: 'Jon Snow',
          value: '@Jon Snow',
          suggestion: '@Jon Snow' // Can be either string or react component
        },
        {
          key: 'John Evans',
          value: '@John Evans',
          suggestion: '@John Evans'
        },
        {
          key: 'Daenerys Targaryen',
          value: '@Daenerys Targaryen',
          suggestion: '@Daenerys Targaryen'
        },
        {
          key: 'Cersei Lannister',
          value: '@Cersei Lannister',
          suggestion: '@Cersei Lannister'
        },
        {
          key: 'Tyrion Lannister',
          value: '@Tyrion Lannister',
          suggestion: '@Tyrion Lannister'
        },
      ]
    };

    this.suggestionsPlugin = SuggestionsPlugin({
      trigger: '@',
      capture: /@([\w]*)/,
      suggestions: async () => {
        const data = this.state.value.document.data;
        const names = data.map(x => x.name).filter(x => !!x).toArray();
        const suggestions = names.map(name => { return { key: name, value: name, suggestion: name }; });
        return suggestions;
      },
      onEnter: (suggestion: any, change: Change, editor: Editor) => {
        const { anchorText, anchorOffset } = editor.value;
  
        const text = anchorText.text;
  
        let index = { start: anchorOffset - 1, end: anchorOffset };
  
        if (text[anchorOffset - 1] !== '@') {
          index = getCurrentWord(text, anchorOffset - 1, anchorOffset - 1) as { start: number, end: number };
        }
  
        const newText = `${text.substring(0, index.start)}${suggestion.value} `;
  
        change
          .deleteBackward(anchorOffset)
          .insertText(newText);
        
        return true;
      }
    });
  
    this.plugins = [
      // StickyInlines({
      //   allowedTypes: ['hz-num'],
      //   canBeEmpty: true,
      //   hasStickyBoundaries: true,
      //   stickOnDelete: true,
      // }),
      // this.suggestionsPlugin
    ];
  }
 
  onPersist = (state?: { value: Value }) => {
    console.log('onPersist()');

    const value = (state) ? state.value : this.state.value;

    const options = { preserveData: true, preserveDecorations: true };
    const json = value.toJSON(options);
    const content = JSON.stringify(json);

    // localStorage.setItem('content', content);

    // console.log('document', this.state.value);
    // console.log('values', this.state.value.data);
  }

  onSave = (state: { value: Value }) => {
    console.log('onSave()', this.state.nonce, state.value.document.data);
    this.setState({ value: state.value }, () => {
      console.log('onSave()/done', this.state.nonce, this.state.value.document.data);
    });
    this.onPersist(state);
  }

  static getTrailingNodeByType(root: Node, path: Array<number>, type: string) {
    let lastFound = null;
    let node = root;
    while (true) {
      const i = path.shift();
      if (typeof i === 'undefined') {
        break;
      }

      node = node.nodes.get(i);
      if (typeof node === 'undefined') {
        break;
      }
      if (node.type === 'hz-num') {
        lastFound = node;
      }
    }

    return lastFound;
  }

  // On change, update the app's React state with the new editor value.
  onChange = (changes: Change) => {
    console.log(
      'onChange()',
      '[' + changes.operations.map((x: Operation) => x.type).join(', ') + ']',
      '\n',
      changes,
      changes.value.document.data);
    
    if (!changes.flags.editor) {
      console.warn('  non-editor change');
      const ops = changes.operations as Immutable.List<Operation>;
      for (const op of ops.toArray()) {
        const { type } = op;

        if (type === 'remove_node') {
          const node = op.node;
          const nodeType = node.type as string | undefined;
          if (nodeType && nodeType.startsWith('hz-')) {
            changes.call(Changes.removeUid, node.data.get('uid'));
          }

        } else if (type === 'insert_text' || type === 'remove_text') {
          const path = op.path as Array<number>;
          const root = changes.value.document;

          let lastFound = HzEditor.getTrailingNodeByType(root, path, 'hz-num');
          if (lastFound) {
            const uid = lastFound.data.get('uid');
            const x = parseFloat(lastFound.getText());
            changes.call(Changes.setUidValue, uid, x);
          }
        }
      }
    } else {
      console.warn('  editor change!');
    }
    // const valueUpdated = value.set('decorations', Range.createList(decorations));
    
    this.onSave(changes);
  }

  static initialTransform = (initialValue: Value) => {
    const getBlock = (v: Value) => v.document.getBlocksByType('paragraph').first();
    const regex = /\d+/;
      
    let change = initialValue.change();
    let lastSiblingKey: string | null = null;
    while (true) {
      const { value } = change;
      const block = getBlock(value);

      let span: Text;
      if (lastSiblingKey === null) {
        span = block.getTexts().first();
      } else {
        span = block.getNextSibling(lastSiblingKey) as any as Text;
      }
      if (!span) {
        break;
      }

      const source = span.text;
      const matches = regex.exec(source);
      if (matches === null) {
        break;
      }
      const input = matches[0];
      const length = input.length;
      const i = matches.index;

      const uid = uuid();
      const x = parseFloat(input);
      
      const range = Range.create({
        anchorKey: span.key,
        anchorOffset: i,
        focusKey: span.key,
        focusOffset: i + length,
      });

      const inline = Inline.create({
        type: 'hz-num',
        data: { uid },
        nodes: [ Text.create(input) ]
      });
      lastSiblingKey = inline.key;

      change = change
        .call(Changes.setUidValue, uid, x)
        .insertInlineAtRange(range, inline);
    }

    return change;
  }

  onInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const search = event.target.value;
    const getBlock = (v: Value) => v.document.getBlocksByType('paragraph').first();
    const decorations = new Array<Partial<RangeAttrs>>();

    // HACK
    if (search === 's' || search === 'm') {
      event.target.value = '';
      
      let change = this.state.value.change();
      let spans = change.value.document.getTextsAsArray();
      
      for (let span of spans) {
        let { key, text } = span;
        let actions = Array<(c: Change) => void>();
        let matches: RegExpExecArray | null;
        const regex = /FOOBAR/g;
        while (matches = regex.exec(text)) {
          const input = matches[0];
          const length = input.length;

          const range = Range.create({
            anchorKey: key,
            anchorOffset: matches.index,
            focusKey: key,
            focusOffset: matches.index + length,
          });

          if (search === 's') {
            change = change.insertInlineAtRange(range, {
              type: 'hz-bold',
              nodes: [ Text.create(input) ]
            });
          } else {
            change = change.addMarkAtRange(range, 'bold');
          }
        }
      }

      this.onSave(change);
    }

    // highlightSearchText(event);
  }

  highlightSearchText = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = this.state;
    const search = event.target.value;
    const texts = value.document.getTexts();
    const decorations = new Array<Partial<RangeAttrs>>();

    console.log('text: ', texts);
    texts.forEach(node => {
      if (typeof node === 'undefined') {
        return;
      }

      const { key, text } = node;
      const parts = text.split(search);
      let offset = 0;

      parts.forEach((part, i) => {
        if (i !== 0) {
          decorations.push({
            anchorKey: key,
            anchorOffset: offset - search.length,
            focusKey: key,
            focusOffset: offset,
            marks: [{ type: 'highlight' }],
          });
        }

        offset = offset + part.length + search.length;
      });
    });

    // setting the `save` option to false prevents this change from being added
    // to the undo/redo stack and clearing the redo stack if the user has undone
    // changes.

    const change = value
      .change()
      .setOperationFlag('save', false)
      .setValue({ decorations })
      .setOperationFlag('save', true);

    this.onSave(change);
  }

  componentWillUpdate(nextProps: HzEditorProps, nextState: HzEditorState) {
    // console.log('NumberSlider/componentWillUpdate', nextProps, nextState);
    console.log('HzEditor/componentWillUpdate', nextState.value.data);
  }

  onKeyDown = (event: React.KeyboardEvent<JSX.Element>, change: Change) => {
    
    if (event.key === 'Escape') {
      console.log('onKeyDown/escape');
      
      const inline = Inline.create({
        type: 'hz-formula',
        data: { uid: uuid() },
        nodes: [ Text.create(' ') ] // HACK: an ampty text node will get merged down
      });

      change
        .insertInline(inline)
        .collapseToStartOf(inline);
    
    } else if (event.key === 'Backspace') {
      console.log('onKeyDown/backspace');
      
      const cursor = change.value.selection;
      if (!cursor.isCollapsed) {
        // TODO: Handled partially selected or force selecting the entire thing
        return;
      }

      const prevSibling = change.value.document.getPreviousSibling(cursor.startKey);
      if (!(prevSibling && prevSibling.type === 'hz-ref')) {
        return;
      }

      if (!cursor.isAtEndOf(prevSibling)) {
        // BUG: Broken
        return;
      }

      event.preventDefault();
      change
        .removeNodeByKey(prevSibling.key)
        .insertText(prevSibling.text.slice(0, -1));

    } else if (event.key === 'Tab') {
      console.log('onKeyDown/tab');

      const cursor = change.value.selection;
      const key = cursor.endKey;
      const span = change.value.document.getNode(key);
      if (span === null) {
        return;
      }

      const text = span.text as string;
      let match: RegExpExecArray | null;
      if ((match = /@[\w-]+/.exec(text)) !== null) {
        event.preventDefault();

        const input = match[0];
        const i = match.index;
        const length = input.length;

        console.log('onKeyDown/Tab', { input });

        // Ensure the cursor beforehand is at the end of the number
        if (cursor.endOffset !== i + length) {
          return;
        }

        const uid = uuid();
        const refUnode = Changes.getUnodeByName(change.value, input.trim());
        const found = !!refUnode;
        
        const range = Range.create({
          anchorKey: span.key,
          anchorOffset: i,
          focusKey: span.key,
          focusOffset: i + length,
        });

        const inline = Inline.create({
          type: 'hz-ref',
          data: Object.assign({ uid }),
          nodes: [ Text.create(input) ]
        });

        change = change
          .call(
            Changes.setUnode,
            uid,
            RefUnode({
              type: 'hz-ref',
              ref: (refUnode) ? refUnode.uid : null
            }))
          .insertInlineAtRange(range, inline);
      }

    } else if (event.key === ' ') {
      const cursor = change.value.selection;
      const key = cursor.endKey;
      const span = change.value.document.getNode(key);
      if (span === null) {
        return;
      }

      const text = span.text as string;
      let match: RegExpExecArray | null;
      if ((match = /\d+/.exec(text)) !== null) {
        const input = match[0];
        const i = match.index;
        const length = input.length;

        // Ensure the cursor beforehand is at the end of the number
        if (cursor.endOffset !== i + length) {
          return;
        }

        const uid = uuid();
        const x = parseFloat(input);
        
        const range = Range.create({
          anchorKey: span.key,
          anchorOffset: i,
          focusKey: span.key,
          focusOffset: i + length,
        });

        const inline = Inline.create({
          type: 'hz-num',
          data: { uid },
          nodes: [ Text.create(input) ]
        });

        change = change
          .call(Changes.setUidValue, uid, x)
          .insertInlineAtRange(range, inline);

        return true;
      }
    }

    return;
  }

  renderNode = (props: any) => {
    console.log(`HzEditor/renderNode/${props.node.type}`, props);

    const { attributes, children } = props; 
    const node: Node = props.node;
    const className = props.isSelected ? 'hz-focused' : 'hz-unfocused';

    switch (node.type) {
      case 'text':
        return <span className={className} {...attributes}>{children}</span>;
      case 'code':
        return (
          <pre {...attributes}>
            <code>{children}</code>
          </pre>
        );
      case 'paragraph':
        return <div className="hz-line" {...attributes}>{children}</div>;
      case 'quote':
        return <blockquote {...attributes}>{children}</blockquote>;
      case 'hz-bold':
        return <strong {...attributes}>{children}</strong>;
      case 'hz-num':
        return (
          <HzNum className={className} {...props}>
            {children}
          </HzNum>
        );
      case 'hz-formula':
        return (
          <BorderedSpan className={className} {...props}>
            {children}
          </BorderedSpan>
        );
      case 'hz-ref':
        return (
          <HzRef className={className} {...props}>
            {children}
          </HzRef>
        );
      default:
        return <div/>;
    }
  }

  renderMark = (props: any) => {
    const { children, mark } = props;
    console.log('mark', mark);
    switch (mark.type) {
      case 'bold':
        return <strong>{children}</strong>;
      case 'italic':
        return <em>{children}</em>;
      case 'underline':
        return <u>{children}</u>;
      case 'highlight':
        return <span style={{ backgroundColor: '#ffeeba' }}>{children}</span>;
      default:
        return <div />;
    }
  }

  // Render the editor.
  render() {
    const document = this.state.value.document;
    const selection = this.state.value.selection;

    const toDebugJSON = (node: Node) => {
      return {
        uid: node.data.get('uid'),
        type: node.type,
        key: node.key,
        text: node.text
      };
    };

    const nextSibling = document.getNextSibling(selection.endKey);
    const next =
      (nextSibling)
      ? toDebugJSON(nextSibling)
      : null;

    const prevSibling = document.getPreviousSibling(selection.startKey);
    const prev =
      (prevSibling)
      ? toDebugJSON(prevSibling)
      : null;

    const length =
      (selection.isCollapsed)
      ? (document.getNode(selection.endKey) as Node).text.length
      : null;

    const opts = {
      preserveKeys: true,
      preserveData: true
    };

    const stringify = (obj: any) =>
      (obj) ? JSON.stringify(obj, null, 2) : null;

    return (
      <div>
        <input onChange={this.onInputChange} />
        <Editor
          style={{'flex-grow': '1'}}
          value={this.state.value}
          onChange={this.onChange}
          onKeyDown={this.onKeyDown}
          renderNode={this.renderNode}
          renderMark={this.renderMark}
          plugins={this.plugins}
          // decorateNode={this.decorateNode}
        />
        <div className="hz-debug-panel">
          <div>
            <strong>Selection</strong>
            <pre>
              <code>{stringify(Object.assign(selection.toJSON(), { length }))}</code>
            </pre>
          </div>
          <div>
            <strong>Previous Sibling</strong>
            <pre>
              <code>{stringify(prev) || 'no prev sibling'}</code>
            </pre>
          </div>
          <div>
            <strong>Next Sibling</strong>
            <pre>
              <code>{stringify(next) || 'no next sibling'}</code>
            </pre>
          </div>
        </div>
        <pre>
          <code>
            {stringify(this.state.value.document.toJSON(opts))}
          </code>
        </pre>
      </div>);
  }
}
