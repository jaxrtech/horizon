import * as React from 'react';
import { Slider, Popover, Position, PopoverInteractionKind } from '@blueprintjs/core';
import { Editor } from 'slate-react';

import { BorderedSpan } from '../BorderedSpan';
import { LabeledInput } from '../LabeledInput';
import './index.css';

interface NumberSliderProps {
  className?: string;
  name?: string;
  value?: number;
  onChangeName: (name: string) => void;
  onChangeValue: (value: number) => void;
}

interface NumberSliderState {
  value: number | undefined;
  name: string | undefined;
}

export class NumberSlider extends React.Component<NumberSliderProps, NumberSliderState> {

  constructor(props: NumberSliderProps) {
    super(props);
    this.state = { value: props.value, name: props.name };
  }

  handleChangeValue = (value: number) => {
    this.setState({ value });
    this.props.onChangeValue(value);
  }

  handleChangeName = (e: React.ChangeEvent<HTMLInputElement>) => {
    const name = e.target.value;
    this.setState({ name });
    this.props.onChangeName(name);
  }

  render() {
    return (
      <BorderedSpan className={this.props.className}>
        <Popover
          position={Position.TOP_LEFT}
          interactionKind={PopoverInteractionKind.HOVER}
        >
          <span>
            {this.props.children}
          </span>
          <div>
            <LabeledInput
              label="Name"
              onChange={this.handleChangeName}
              value={this.state.name || ''}
            />
            <Slider
              min={0}
              max={2000}
              stepSize={1}
              labelStepSize={500}
              value={this.state.value}
              onChange={this.handleChangeValue}
            />
          </div>
        </Popover>
      </BorderedSpan>
    );
  }
}