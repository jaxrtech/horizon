import * as React from 'react';
import * as Changes from '../../util/changes';
import { EditorProps } from '../../util/editor-props';
import { RefUnode, NumUnode } from '../../util/changes';
import { Popover, Position, PopoverInteractionKind } from '@blueprintjs/core';
import { LabeledInput } from '../LabeledInput';

export interface HzRefProps extends EditorProps { }

export interface HzRefState {
  value: number | undefined;
}

export class HzRef extends React.Component<HzRefProps, HzRefState> {
  constructor(props: HzRefProps) {
    super(props);
    this.state = { value: this._refValue };
  }

  private _refresh = () => {
    this.setState({ value: this._refValue });
  }

  private get _uid() {
    return this.props.node.data.get('uid');
  }

  private get _editorValue() {
    return this.props.editor.value;
  }

  private get _refValue() {
    const uid = this._uid;
    const refUnode = Changes.getUnode<RefUnode>(this._editorValue, uid);
    if (typeof refUnode === 'undefined') {
      console.error('invalid uid', uid);
      return;
    }

    const ref = refUnode.ref; 
    if (!ref) {
      return;
    }

    const numUnode = Changes.getUnode<NumUnode>(this._editorValue, ref);
    if (typeof numUnode === 'undefined') {
      console.error('invalid ref uid', uid);
      return;
    }

    return numUnode.value;
  }

  render() {
    const unode = Changes.getUnode(this.props.editor.value, this.props.node.data.get('uid')) as RefUnode;
    const found = !!unode.ref;
    const className = 'pt-tag pt-round' + (found ? '' : ' pt-intent-danger');
    
    return (
      <Popover
        position={Position.TOP}
        interactionKind={PopoverInteractionKind.HOVER}
        popoverWillOpen={this._refresh}
      >
        <span {...this.props.attributes} className={className}>
          {this.props.children}
        </span>
        <div>
          {this.state.value}
        </div>
      </Popover>
    );
  }
}