import * as React from 'react';
import './index.css';

interface BorderedSpanProps {
  className?: string;
}

export class BorderedSpan extends React.Component<BorderedSpanProps, {}> {
  constructor(props: BorderedSpanProps) {
    super(props);
  }

  render() {
    const className = ['hz-border', this.props.className || ''].join(' ');
    return (
      <span className={className}>
        {this.props.children}
      </span>
    );
  }
}