import * as React from 'react';

interface LabeledInputProps {
  label: string;
  onChange: React.ChangeEventHandler<HTMLInputElement>;
  value: string;
}

interface LabeledInputState {
  value: string;
}

export class LabeledInput extends React.Component<LabeledInputProps, LabeledInputState> {
  
  constructor(props: LabeledInputProps) {
    super(props);
    this.state = { value: props.value };
  }

  componentWillMount() {
    console.log('LabeledInput/componentWillMount', { props: this.props, state: this.state });
  }

  componentWillUnmount() {
    console.log('LabeledInput/componentWillUnmount', { props: this.props, state: this.state });    
  }

  handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ value: e.target.value });
    this.props.onChange(e);
  }

  componentWillReceiveProps(nextProps: LabeledInputProps, nextContext: any) {
    console.log('LabeledInput/componentWillReceiveProps', { props: this.props, nextProps });
  }

  render() {
    return (
      <label className="pt-label">
        {this.props.label}:
        <span className="pt-text-muted">(required)</span>
        <input
          {...this.props}
          className="pt-input"
          type="text"
          dir="auto"
          value={this.state.value}
          onChange={this.handleChange}
        />
      </label>
    );
  }
}