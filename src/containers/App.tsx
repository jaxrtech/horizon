import * as React from 'react';
import { KeyboardEvent, ChangeEventHandler } from 'react';
import {
  Button,
  Intent,
  Popover,
  Slider,
  Position,
  ISliderProps
} from '@blueprintjs/core';

import { Editor } from 'slate-react';
import {
  Value,
  Node,
  Block,
  Mark,
  Text,
  Range,
  Change,
  Inline,
  Operation,
  Data
} from 'slate';
import { RangeAttrs } from 'slate/models/range';
import Html from 'slate-html-serializer';
import StickyInlines from 'slate-sticky-inlines';
import AutoReplace from 'slate-auto-replace';

import { Key } from 'ts-keycode-enum';
import * as Immutable from 'immutable';

import { DataBroadcast } from '../util/broadcast';
import { NumberSlider } from '../components/NumberSlider';
import * as Changes from '../util/changes';
import './App.css';
import { HzEditor } from '../components/HzEditor';

interface AppProps { }

interface AppState {
  a: number;
  line: string;
}

class App extends React.Component<AppProps, AppState> {

  constructor() {
    super({});
    this.state = { a: 0, line: 'x = a + 5' };
  }

  onChange() {
    return (value: number) => this.setState({ a: value });
  }

  handleKeyPress() {
    // from https://stackoverflow.com/a/13127566

    const toAscii = {
      188: ',',
      106: '*',
      107: '+',
      109: '-',
      110: '.',
      111: '/',
      173: '-',
      190: '.',
      191: '/',
      192: '`',
      220: '\\',
      222: '\'',
      219: '[',
      221: ']',
      187: '=',
      186: ';',
      189: '-' 
    };

    const shiftUps = {
      96: '~',
      49: '!',
      50: '@',
      51: '#',
      52: '$',
      53: '%',
      54: '^',
      55: '&',
      56: '*',
      57: '(',
      48: ')',
      45: '_',
      61: '+',
      91: '{',
      93: '}',
      92: '|',
      59: ':',
      39: '"',
      44: '<',
      46: '>',
      47: '?'
    };

    return (e: KeyboardEvent<HTMLElement>) => {
      const prevLine = this.state.line;

      console.log(`${e.which} / ${e.key} | keyCode = ${e.keyCode}, charCode = ${e.charCode}`);
      let k = e.which;

      if (k === Key.Backspace) {
        this.setState({ line: prevLine.substring(0, prevLine.length - 1) });
        return;
      }

      // Anything other than:
      //   - SPACE (32), or
      //   - lower `0` (48) && higher than `Z` (90) in ASCII
      //
      // are mapped to `toAscii` or are calcuated using `shiftKey` offset
      const isNum = (key: number) => k >= Key.Zero && k <= Key.Nine;
      const isAlpha = (key: number) => k >= Key.A && k <= Key.Z;
      const isAlphaNum = (key: number) => isAlpha(key) || isNum(key);
      const isPrintable = (key: number) => k === Key.Space || isAlphaNum(key);
      const isNumpad = (key: number) => k >= Key.Numpad0 && k <= Key.Numpad9;

      function translate(key: number): string | null {
        const NUMPAD_OFFSET = -Key.Numpad0 + 48 /* ZERO */;
        const LOWERCASE_OFFSET = 32;
        if (toAscii.hasOwnProperty(k)) {
          return toAscii[k];
        } else if (!e.shiftKey && isAlpha(k)) {
          return String.fromCharCode(k + LOWERCASE_OFFSET);
        } else if (e.shiftKey && shiftUps.hasOwnProperty(k)) {
          return shiftUps[k];
        } else if (isNumpad(k)) {
          return String.fromCharCode(k + NUMPAD_OFFSET);
        } else if (isPrintable(k)) {
          return String.fromCharCode(k);
        } else {
          if (k === Key.LeftArrow) {
            console.log('left');
          } else if (k === Key.RightArrow) {
            console.log('right');
          } else if (k === Key.UpArrow) {
            console.log('up');
          } else if (k === Key.DownArrow) {
            console.log('down');
          }
          return null;
        }
      }

      const c = translate(k);
      if (!c) {
        return;
      }

      console.log('-> ' + c);
      this.setState({ line: prevLine + c });
    };
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">horizon<sup>indev</sup></h1>
        </header>
        <section className="App-main">
          <div className="App-breadcrumb">
            example.hz
          </div>
          <article className="App-article" tabIndex={0} onKeyDown={this.handleKeyPress()}>
            <div>a =
              <Popover position={Position.TOP_LEFT}>
                <span className="editable-value">{this.state.a.toFixed(2)}</span>
                <Slider
                    min={0}
                    max={10}
                    stepSize={0.1}
                    labelStepSize={10}
                    onChange={this.onChange()}
                    value={this.state.a}
                />
              </Popover>
            </div>
            <p onKeyPress={this.handleKeyPress()}>{this.state.line}<span className="cursor"/>
              <span className="inline-answer">x = {(5 + this.state.a).toFixed(2)}</span></p>
          </article>
          <hr/>
          <article className="App-article">
            <HzEditor />
          </article>
        </section>
      </div>
    );
  }
}

export default App;
