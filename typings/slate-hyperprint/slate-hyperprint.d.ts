
declare module 'slate-hyperprint' {
    import { Node } from 'slate';

    type SlateModel = Node;

    type Options = {
        // True to print keys
        preserveKeys: boolean,
        // True to output a strict representation of the document (print empty text nodes for example)
        strict: boolean,
        // Prettier config to use
        prettier: Object
    };

    const DEFAULT_OPTIONS: Options;
    
    function hyperprint(
        model: SlateModel,
        optionsParam?: Options
    ): string;

    module hyperprint {
        function log(model: SlateModel, optionsParam?: Options): void;
    }

    export default hyperprint;
}