
declare module 'slate-sticky-inlines' {
    export interface Options {
        allowedTypes: Array<string>;
        bannedTypes: Array<string>;
        hasStickyBoundaries: boolean;
        canBeEmpty: boolean;
        stickOnDelete: boolean;
    }

    /**
     * Cross Boundaries on Left Arrow, or Right arrow
     * when on the Start or End of an inline boundary
     * can delete all the text inside and still type to add more
     *
     * @param {Object} options
     *   @property {Array} allowedTypes (optional)
     *   @property {Array} bannedTypes (optional)
     *   @property {Boolean} hasStickyBoundaries (optional)
     *   @property {Boolean} canBeEmpty (optional)
     *   @property {Boolean} stickOnDelete (optional)
     * @return {Object} plugin
     */

    function StickyInlines(options: Partial<Options>): any;

    export default StickyInlines;
}