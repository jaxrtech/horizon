
declare module '@jaxrtech/slate-suggestions' {
  function SuggestionsPlugin(opts: any): any

  export default SuggestionsPlugin;
}