
declare module 'slate-auto-replace' {
    import { Change } from "slate";
    import { Editor } from "slate-react";
    
    export interface Options {
        /** The trigger to match the inputed character against—either a character like a or a key name like enter or tab. */
        trigger: string | RegExp | ((input: KeyboardEvent) => boolean);

        /** A function to apply the desired transform. It will be called with transform, e, matches, editor from the event handler. The matching (before and after) text is deleted but are accessible inside matches.before and matches.after. */
        transform: (transform: Change, e: KeyboardEvent, matches: { before: RegExpExecArray, after: RegExpExecArray }, editor: Editor) => void;
        
        /** An optional regexp that must match the text after the trigger for the replacement to occur. Any capturing groups in the regexp will be deleted from the text content, but is accessible in matches parameter in the transform function. */
        after?: RegExp;

        /** An optional regexp that must match the text before the trigger for the replacement to occur. Any capturing groups in the regexp will be deleted from the text content, but is accessible in matches parameter in the transform function. */
        before?: RegExp;
        
        /** An optional block matcher to ignore triggers inside. If passed an array or string it will match by node.type. */
        ignoreIn?: Function | Array<string> | string

        /** An optional block matcher to only replace triggers inside. If passed an array or string it will match by node.type. */
        onlyIn?: Function | Array<string> | string
    }

    function AutoReplace(opts: Options): any;

    export default AutoReplace;
}